#include <Windows.h>
#include <Fltuser.h>
#include <stdio.h>
#include "common.h"

VOID
MainValidated(
    LPCSTR extension
)
{
    HANDLE portHandle = INVALID_HANDLE_VALUE;
    HRESULT result = S_OK;
    DWORD receivedBytes;
    USER_MESSAGE message;
    DRIVER_MESSAGE driverMessage;
    ACCESS_MESSAGE accessMessage;
    int response;

    printf("Trying to connect to driver...\n");
    result = FilterConnectCommunicationPort(
        WDM_DRIVER_FILTER_PORT_NAME,
        0,
        NULL, 
        0,
        NULL,
        &portHandle
    );
    if (result != S_OK)
    {
        printf("Could not connect to the driver!\n");
        goto cleanup;
    }
    printf("Connection established!\n");

    message.command = MESSAGE_COMMAND_EXTENSION_SPECIFIED;
    strcpy_s(message.contents, _ARRAYSIZE(message.contents), extension);

    // send the extension
    result = FilterSendMessage(
        portHandle,
        &message,
        sizeof(message),
        NULL,
        0,
        &receivedBytes
    );

    if (result != S_OK)
    {
        printf("Could not send message!\n");
        goto cleanup;
    }

    printf("Extension sent!\n");

    while (TRUE)
    {
        // wait for notification from filter
        result = FilterGetMessage(
            portHandle,
            (PFILTER_MESSAGE_HEADER)&driverMessage, // we know that the first field in the structure is the message header.
            sizeof(driverMessage),
            NULL
        );

        if (result != S_OK)
        {
            printf("Get message failed!\n");
            goto cleanup;
        }

        driverMessage.body.filePath[driverMessage.body.pathLength] = '\0';

        // ask the user if he/she wants to allow access to the file.
        response = MessageBoxW(NULL, driverMessage.body.filePath, L"Do you want to allow access to this file?", MB_YESNO);
        if (response == IDYES)
        {
            accessMessage.allow = 1;
        }
        else
        {
            accessMessage.allow = 0;
        }

        accessMessage.header.MessageId = driverMessage.header.MessageId;
        accessMessage.header.Status = 0;

        // send the reply to the user.
        result = FilterReplyMessage(
            portHandle, 
            (PFILTER_REPLY_HEADER)&accessMessage, // we know that the first field in the structure is the header.
            sizeof(accessMessage.header) + sizeof(accessMessage.allow)
        );
        if (result != S_OK)
        {
            printf("Could not reply to message\n");
            goto cleanup;
        }
    }

cleanup:
    if (portHandle != INVALID_HANDLE_VALUE)
    {
        CloseHandle(portHandle);
    }
}

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        printf("Usage: %s <extension> (without the dot (.) )\n", argv[0]);
        return 1;
    }

    char* extension = argv[1];
    if (strlen(extension) >= MESSAGE_CONTENTS_SIZE)
    {
        printf("extension cannot be larger than %d\n", MESSAGE_CONTENTS_SIZE);
        return 1;
    }

    MainValidated(extension);

    return 0;
}
