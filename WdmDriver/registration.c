#include "WdmDriver.h"

CONST FLT_OPERATION_REGISTRATION Callbacks[] = 
{
    {
        IRP_MJ_CREATE,
        0,
        WdmDriverPreCreateCallback,
        NULL
    },

    {
        IRP_MJ_OPERATION_END
    }
};

CONST FLT_REGISTRATION FltRegistration =
{
    sizeof(FLT_REGISTRATION), // size
    FLT_REGISTRATION_VERSION, // version
    0, // flags

    NULL, // contexts
    Callbacks, // Operation callbacks
    WdmDriverFilterUnload // filter unload
};
