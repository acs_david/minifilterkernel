#pragma once

#ifndef WDMDRIVER_H
#define WDMDRIVER_H

#include <fltKernel.h>

EXTERN_C CONST FLT_REGISTRATION FltRegistration;


NTSTATUS
FLTAPI 
WdmDriverFilterUnload (
    _In_ FLT_FILTER_UNLOAD_FLAGS Flags
);

FLT_PREOP_CALLBACK_STATUS
FLTAPI 
WdmDriverPreCreateCallback(
    _Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects,
    _Outptr_result_maybenull_ PVOID *CompletionContext
    );

#endif // WDMDRIVER_H

