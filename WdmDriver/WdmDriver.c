/*++

Module Name:

    WdmDriver.c

Abstract:

    This is the main module of the WdmDriver miniFilter driver.
    This driver will receive an extension from a user mode application and start to 
    monitor file operation looking for files with the specified extension.

    if a file with the extension is found, then the user application is notified, which will send back a decision.
    Using the decision: ALLOW/DENY we will take the corresponding action: block the request or let it complete.

Environment:

    Kernel mode

--*/

#include <fltKernel.h>
#include <ntstrsafe.h>
#include <dontuse.h>
#include <suppress.h>

#include "WdmDriver.h"
#include "common.h"

typedef struct _EXTENSION_DATA{
    UNICODE_STRING extension;
    BOOLEAN initializedExtension;
    EX_PUSH_LOCK extensionLock;
}EXTENSION_DATA, *PEXTENSION_DATA;

typedef struct _WDM_DATA{
    PDRIVER_OBJECT driverObject;
    PFLT_FILTER filter;
    PFLT_PORT serverPort;
    PFLT_PORT clientPort;
    EXTENSION_DATA extensionData;
}WDM_DATA, *PWDM_DATA;

// global variables, all initialized to null
WDM_DATA WdmData;

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD DriverUnload;

NTSTATUS
DriverEntry (
    _In_ PDRIVER_OBJECT DriverObject,
    _In_ PUNICODE_STRING RegistryPath
    );

VOID
DriverUnload(
    _In_ struct _DRIVER_OBJECT *DriverObject
);

NTSTATUS
FLTAPI 
WdmDriverConnectCallback(
      _In_ PFLT_PORT ClientPort,
      _In_opt_ PVOID ServerPortCookie,
      _In_reads_bytes_opt_(SizeOfContext) PVOID ConnectionContext,
      _In_ ULONG SizeOfContext,
      _Outptr_result_maybenull_ PVOID *ConnectionPortCookie
      );

VOID
FLTAPI 
WdmDriverDisconnectCallback(
      _In_opt_ PVOID ConnectionCookie
  );

NTSTATUS
FLTAPI WdmDriverMessageCallback (
    _In_opt_ PVOID PortCookie,
    _In_reads_bytes_opt_(InputBufferLength) PVOID InputBuffer,
    _In_ ULONG InputBufferLength,
    _Out_writes_bytes_to_opt_(OutputBufferLength,*ReturnOutputBufferLength) PVOID OutputBuffer,
    _In_ ULONG OutputBufferLength,
    _Out_ PULONG ReturnOutputBufferLength
    );

/*************************************************************************
    Extension data routines.
*************************************************************************/

VOID
InitializeExtensionLock(
    _In_ PEXTENSION_DATA extensionData
)
{
    FltInitializePushLock(&extensionData->extensionLock);
}

VOID
DeleteExtensionLock(
    _In_ PEXTENSION_DATA extensionData
)
{
    FltDeletePushLock(&extensionData->extensionLock);
}

VOID
DeleteExtensionData(
    _In_ PEXTENSION_DATA extensionData
)
{
    FltAcquirePushLockExclusive(&extensionData->extensionLock);
    if (extensionData->initializedExtension)
    {
        RtlFreeUnicodeString(&extensionData->extension);
    }
    extensionData->initializedExtension = FALSE;
    FltReleasePushLock(&extensionData->extensionLock);
}

BOOLEAN
IsExtensionInitialized(
    _In_ PEXTENSION_DATA extensionData
)
{
    BOOLEAN extensionInitialiazed = FALSE;

    FltAcquirePushLockShared(&extensionData->extensionLock);
    extensionInitialiazed = extensionData->initializedExtension;
    FltReleasePushLock(&extensionData->extensionLock);

    return extensionInitialiazed;
}

BOOLEAN 
ExtensionMatches(
    _In_ PEXTENSION_DATA extensionData,
    _In_ PUNICODE_STRING extensionToCheck
)
{
    BOOLEAN matches = FALSE;

    FltAcquirePushLockShared(&extensionData->extensionLock);
    matches = RtlEqualUnicodeString(&extensionData->extension, extensionToCheck, TRUE);
    FltReleasePushLock(&extensionData->extensionLock);

    return matches;
}

NTSTATUS
InitializeExtensionWithAnsiString(
    _In_ PEXTENSION_DATA extensionData,
    _In_ LPCSTR ansiToCopy
)
{
    NTSTATUS status = STATUS_SUCCESS;
    ANSI_STRING ansiString;

    FltAcquirePushLockExclusive(&extensionData->extensionLock);

    RtlInitAnsiString(&ansiString, ansiToCopy);
    status = RtlAnsiStringToUnicodeString(&extensionData->extension, &ansiString, TRUE);
    if (!NT_SUCCESS(status))
    {
        return status;
    }
    extensionData->initializedExtension = TRUE;

    FltReleasePushLock(&extensionData->extensionLock);

    return STATUS_SUCCESS;
}

VOID
ReleaseExtensionForRead(
    _In_ PEXTENSION_DATA extensionData
)
{
    FltReleasePushLock(&extensionData->extensionLock);
}

/*************************************************************************
    Initialization and unload routines.
*************************************************************************/

NTSTATUS
DriverEntry (
    _In_ PDRIVER_OBJECT DriverObject,
    _In_ PUNICODE_STRING RegistryPath
    )
/*++

Routine Description:

    This is the initialization routine for this miniFilter driver.  This
    registers with FltMgr and initializes all global data structures.

Arguments:

    DriverObject - Pointer to driver object created by the system to
        represent this driver.

    RegistryPath - Unicode string identifying where the parameters for this
        driver are located in the registry.

Return Value:

    Routine can return non success error codes.

--*/
{
    // Declare & Initialize local variables
    NTSTATUS status = STATUS_DEVICE_NOT_READY;
    PSECURITY_DESCRIPTOR securityDescriptor = NULL;
    OBJECT_ATTRIBUTES objectAttributes;
    UNICODE_STRING portNameString;

    UNREFERENCED_PARAMETER(DriverObject);
    UNREFERENCED_PARAMETER(RegistryPath);

    // Use __try {} __finally {} & __leave in order to have a single exit point
    // used for freeing up resources or uninitialization in case of failure
    __try
    {
        // let's print something in debugger
        DbgPrint("hello, world\n");

        // Setup driver unload routine
        DriverObject->DriverUnload = DriverUnload;
        WdmData.driverObject = DriverObject;

        InitializeExtensionLock(&WdmData.extensionData);

        status = FltRegisterFilter(DriverObject, &FltRegistration, &WdmData.filter);
        if (!NT_SUCCESS(status))
        {
            DbgPrint("Registering failed with: %x\n", status);
            __leave;
        }

        status = FltBuildDefaultSecurityDescriptor(
            &securityDescriptor,
            FLT_PORT_ALL_ACCESS
        );

        if (!NT_SUCCESS(status))
        {
            DbgPrint("Could no build sec descriptor: %x\n", status);
            __leave;
        }

        // preparation to create port: initialize object attributes and port name
        RtlInitUnicodeString(&portNameString, WDM_DRIVER_FILTER_PORT_NAME);

        InitializeObjectAttributes(
            &objectAttributes,
            &portNameString,
            OBJ_KERNEL_HANDLE,
            NULL,
            securityDescriptor
        );

        // create the communication port, allow a single connection
        status = FltCreateCommunicationPort(
            WdmData.filter,
            &WdmData.serverPort,
            &objectAttributes,
            NULL,
            WdmDriverConnectCallback,
            WdmDriverDisconnectCallback,
            WdmDriverMessageCallback,
            1
        );

        if (!NT_SUCCESS(status))
        {
            DbgPrint("Could not create communication port with error: %x\n", status);
            __leave;
        }


        status = FltStartFiltering(WdmData.filter);
        if (!NT_SUCCESS(status))
        {
            DbgPrint("Starting the filtering failed with %x\n", status);
            __leave;
        }


        DbgPrint("Initialization successful\n");
        // If we reached this point, all worked well
        status = STATUS_SUCCESS;
    }
    __finally
    {
        if (!NT_SUCCESS(status))
        {
            DbgPrint("Started cleanup...\n");
            // error occurred, we need to do cleanup
            if (WdmData.serverPort != NULL)
            {
                FltCloseCommunicationPort(WdmData.serverPort);
            }
            if (securityDescriptor != NULL)
            {
                FltFreeSecurityDescriptor(securityDescriptor);
            }

            if (WdmData.filter != NULL)
            {
                FltUnregisterFilter(WdmData.filter);
            }

            DeleteExtensionLock(&WdmData.extensionData);

        }
        DbgPrint("Initialization done\n");
    }

    return status;
}

VOID
DriverUnload(
    _In_ struct _DRIVER_OBJECT *DriverObject
)
{
    UNREFERENCED_PARAMETER(DriverObject);

    // Uninitialize used kernel callbacks & free memory
    DbgPrint("WdmDriver unloading\n");

    // !!! This function cannot fail, the return value is VOID
    // If something is not initialized, your driver will unload and Windows will crash
}

/*************************************************************************
    Callback routines.
*************************************************************************/

NTSTATUS
FLTAPI 
WdmDriverFilterUnload (
    _In_ FLT_FILTER_UNLOAD_FLAGS Flags
)
{
    UNREFERENCED_PARAMETER(Flags);

    /*
     * Free all used resources.
     */
    FltCloseCommunicationPort(WdmData.serverPort);

    /*
     * Unregister from filter manager.
     */
    FltUnregisterFilter(WdmData.filter);

    DeleteExtensionData(&WdmData.extensionData);
    
    return STATUS_SUCCESS;
}

FLT_PREOP_CALLBACK_STATUS
FLTAPI 
WdmDriverPreCreateCallback(
    _Inout_ PFLT_CALLBACK_DATA Data,
    _In_ PCFLT_RELATED_OBJECTS FltObjects,
    _Outptr_result_maybenull_ PVOID *CompletionContext
    )
{
/*++

Routine Description:

    This is the Pre callback function for IRP_MJ_CREATE, which will intercept the file operations with the used defined extension.
    Once a file with the specified extension is detected, we will send a message to the connected UM app waiting for a reply.

Return Value:

    Routine can return FLT_PREOP_SUCCESS_NO_CALLBACK (if we allow access) or FLT_PREOP_COMPLETE (if we deny access)

--*/
    UNREFERENCED_PARAMETER(CompletionContext);

    FLT_PREOP_CALLBACK_STATUS returnStatus = FLT_PREOP_SUCCESS_NO_CALLBACK;
    NTSTATUS status = STATUS_SUCCESS;
    PFLT_FILE_NAME_INFORMATION nameInfo = NULL;
    int allowFileAccess;
    ULONG allowFileAccessSize = sizeof(allowFileAccess);
    DRIVER_MESSAGE_BODY message;

    if (!IsExtensionInitialized(&WdmData.extensionData))
    {
        // check if the we got the extension from the UM app (meaning the client is connected)
        return returnStatus;
    }

    __try
    {
        if (FltObjects->FileObject == NULL)
        {
            __leave;
        }

        // get the name of the file
        status = FltGetFileNameInformation(
            Data,
            FLT_FILE_NAME_NORMALIZED | FLT_FILE_NAME_QUERY_DEFAULT,
            &nameInfo
        );

        if (!NT_SUCCESS(status))
        {
            __leave;
        }

        status = FltParseFileNameInformation(nameInfo);
        if (!NT_SUCCESS(status))
        {
            __leave;
        }

        if (!ExtensionMatches(&WdmData.extensionData, &nameInfo->Extension))
        {
            // check if we are interested in the file by extension.
            __leave;
        }

        DbgPrint("Name: %wZ\n", nameInfo->Name);

        // send the filename to the user mode app
        message.pathLength = min(nameInfo->Name.Length, _ARRAYSIZE(message.filePath));
        RtlStringCchCopyW(message.filePath, _ARRAYSIZE(message.filePath), nameInfo->Name.Buffer);

        status = FltSendMessage(
            WdmData.filter, 
            &WdmData.clientPort, 
            &message, 
            sizeof(message), 
            &allowFileAccess, 
            &allowFileAccessSize, 
            NULL
        );

        if (!NT_SUCCESS(status))
        {
            DbgPrint("Could not send message: %x\n", status);
            __leave;
        }

        DbgPrint("Just got a command: %x for file: %wZ\n", allowFileAccess, nameInfo->Name);
        if (allowFileAccess)
        {
            // just continue normally
            DbgPrint("User allowed file: %wZ\n", nameInfo->Name);
            __leave;
        }

        if (!allowFileAccess)
        {
            DbgPrint("User DENIED access to file: %wZ\n", nameInfo->Name);
            Data->IoStatus.Status = STATUS_ACCESS_DENIED;
            Data->IoStatus.Information = 0;
            returnStatus = FLT_PREOP_COMPLETE;
            __leave;
        }

    }
    __finally
    {
        // free all resources.
        if (nameInfo != NULL)
        {
            FltReleaseFileNameInformation(nameInfo);
        }
    }

    return returnStatus;
}

NTSTATUS
FLTAPI 
WdmDriverConnectCallback(
      _In_ PFLT_PORT ClientPort,
      _In_opt_ PVOID ServerPortCookie,
      _In_reads_bytes_opt_(SizeOfContext) PVOID ConnectionContext,
      _In_ ULONG SizeOfContext,
      _Outptr_result_maybenull_ PVOID *ConnectionPortCookie
      )
{
/*++

Routine Description:

    This function is called when the client connects to the port of the driver.
    We just save the port for further use.

--*/
    UNREFERENCED_PARAMETER(ServerPortCookie);
    UNREFERENCED_PARAMETER(ConnectionContext);
    UNREFERENCED_PARAMETER(SizeOfContext);
    *ConnectionPortCookie = NULL;

    DbgPrint("Client connected\n");

    // just save the port of the connected client.
    WdmData.clientPort = ClientPort;
    return STATUS_SUCCESS;
}

VOID
FLTAPI 
WdmDriverDisconnectCallback(
      _In_opt_ PVOID ConnectionCookie
  )
{
    UNREFERENCED_PARAMETER(ConnectionCookie);
    DbgPrint("Client DISCONNECTED!\n");

    DeleteExtensionData(&WdmData.extensionData);

    FltCloseClientPort(WdmData.filter, &WdmData.clientPort);
}

NTSTATUS
FLTAPI 
WdmDriverMessageCallback (
    _In_opt_ PVOID PortCookie,
    _In_reads_bytes_opt_(InputBufferLength) PVOID InputBuffer,
    _In_ ULONG InputBufferLength,
    _Out_writes_bytes_to_opt_(OutputBufferLength,*ReturnOutputBufferLength) PVOID OutputBuffer,
    _In_ ULONG OutputBufferLength,
    _Out_ PULONG ReturnOutputBufferLength
    )
{
    UNREFERENCED_PARAMETER(PortCookie);
    UNREFERENCED_PARAMETER(OutputBuffer);
    UNREFERENCED_PARAMETER(OutputBufferLength);
    UNREFERENCED_PARAMETER(ReturnOutputBufferLength);

    NTSTATUS status = STATUS_SUCCESS;

    DbgPrint("Message from user mode\n");
    if (InputBufferLength < sizeof(USER_MESSAGE))
    {
        return STATUS_INVALID_BUFFER_SIZE;
    }

    PUSER_MESSAGE message = (PUSER_MESSAGE)InputBuffer;
    switch (message->command)
    {
        case MESSAGE_COMMAND_EXTENSION_SPECIFIED: 
            if (IsExtensionInitialized(&WdmData.extensionData))
            {
                DbgPrint("Extension already specified! client should disconnect before sending extension again\n");
                break;
            }

            status = InitializeExtensionWithAnsiString(&WdmData.extensionData, message->contents);
            if (!NT_SUCCESS(status))
            {
                DbgPrint("Extension: could not be copied\n");
                break;
            }

            DbgPrint("Extension: %wZ\n", WdmData.extensionData.extension);
            break;
        default: 
            DbgPrint("Unknown command\n");
    }

    return STATUS_SUCCESS;
}
